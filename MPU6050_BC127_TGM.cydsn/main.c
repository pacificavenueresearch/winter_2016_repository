/* ========================================
*
*/
#include <project.h>
#include <mpu6050.h>
#include <stdio.h>
#include "Madgwick.h"
#include <math.h>


#define numberOfTests   100

char buf[200]; //just to hold text values in for writing to UART
int i = 0; //for loop increment variable
/*********************************
* BEGIN GYRO
**********************************/
int16_t CAX, CAY, CAZ; //current acceleration values
int16_t CGX, CGY, CGZ; //current gyroscope values
int16_t CT;            //current temperature
   
float   AXoff, AYoff, AZoff; //accelerometer offset values
float   GXoff, GYoff, GZoff; //gyroscope offset values

float   AX, AY, AZ; //acceleration floats
float   GX, GY, GZ; //gyroscope floats
float yaw, pitch, roll;

float q[4] = {1.0f, 0.0f, 0.0f, 0.0f}; // vector to hold quaternion
float PI = 3.14159265358979323846f;

float *ptrAX, *ptrAY, *ptrAZ, *ptrGX, *ptrGY, *ptrGZ;   //Added by DKW
/*********************************
* END GYRO
**********************************/

/*********************************
* BEGIN IMPACT ACCELEROMETER to AUDIO OUT
**********************************/
/*******************************************************************************
*  Helipful data manipulation defines from psoc5 pass thru example
*******************************************************************************/

/* Get 8 bits of 16 bit value. */
#define LO8(x)                  ((uint8) ((x) & 0xFFu))
#define HI8(x)                  ((uint8) ((uint16)(x) >> 8))

/* Get 16 bits of 32 bit value. */
#define LO16(x)                 ((uint16) ((x) & 0xFFFFu))
#define HI16(x)                 ((uint16) ((uint32)(x) >> 16))



#define    IDAC_VAL     (0x33u)
#define    IDAC_CURRENT (120u)

/*******************************************************************************
*   Function Prototypes
*******************************************************************************/

/*******************************************************************************
*   Global Variables
*******************************************************************************/

/* ADC measurement status and result */
int16   adcSample = 0;
int16   adcSampleQpoint = 0;
uint8   idacSet = 0x33;
int32   newRegisterValue = 0x0;
/* sinte table */
 const uint8 sinetable[ 32 ] = {     127, 152, 176, 198, 217, 233, 245, 252,
                                    254, 252, 245, 233, 217, 198, 176, 152,
                                    128, 103,  79,  57,  38,  22,  10,   3,
                                      0,   3,  10,  22,  38,  57,  79, 103 };

uint8 sineptr = 0;
uint8 freqCount = 0;
uint8 modulate = 0;
/* end sine table */


/*********************************
* END IMPACT ACCELEROMETER to AUDIO OUT
**********************************/

/*********************************
* BEGIN BC127
**********************************/


/*********************************
* END BC127
**********************************/

int main()
{ 
	I2C_MPU6050_Start();
	SERIAL_Start();
	
    CyGlobalIntEnable;

	MPU6050_init();
	MPU6050_initialize();
	SERIAL_UartPutString(MPU6050_testConnection() ? "MPU6050 connection successful\n\r" : "MPU6050 connection failed\n\n\r");
    
    SERIAL_UartPutString("Starting to calibrate values from sensor..\n\r");
     
    //Count and average the first n values, defined by numberOfTests above..
    for(i=0; i<numberOfTests; i++)
    {
      sprintf(buf, "Test Number: %d \n\r", i);
      SERIAL_UartPutString(buf);
    
      MPU6050_getMotion6t(&CAX, &CAY, &CAZ, &CGX, &CGY, &CGZ, &CT);
      AXoff += CAX;
      AYoff += CAY;
      AZoff += CAZ;
      GXoff += CGX;
      GYoff += CGY;
      GZoff += CGZ;
       
      sprintf(buf, "AX:%d, AY:%d, AZ:%d || GX:%d, GY:%d, GZ:%d,\t", CAX,CAY,CAZ,CGX,CGY,CGZ);
      SERIAL_UartPutString(buf);
      SERIAL_UartPutString("\n\r");
      CyDelay(25);
    }
    
    AXoff = AXoff/numberOfTests;
    AYoff = AYoff/numberOfTests;
    AZoff = AZoff/numberOfTests;
    GXoff = GXoff/numberOfTests;
    GYoff = GYoff/numberOfTests;
    GZoff = GZoff/numberOfTests;
    
    SERIAL_UartPutString("\n\nTest finished, offset values are shown below\n\n\r");
    sprintf(buf, "AXoff:%d, AYoff:%d, AZoff:%d || GXoff:%d, GYoff:%d, GZoff:%d,\t", (int)AXoff,(int)AYoff,(int)AZoff,(int)GXoff,(int)GYoff,(int)GZoff);
    SERIAL_UartPutString(buf);
    
    while(1)
    {
      //Convert values to G's
      // SERIAL_UartPutString("\r\n\nConverting Values to G\'s\n\n\r");  
        
      MPU6050_getMotion6t(&CAX, &CAY, &CAZ, &CGX, &CGY, &CGZ, &CT);
      AX = ((float)CAX-AXoff)/16384.00;
      AY = ((float)CAY-AYoff)/16384.00; //16384 is just 32768/2 to get our 1G value
      AZ = ((float)CAZ-(AZoff-16384))/16384.00; //remove 1G before dividing
    
      GX = ((float)CGX-GXoff)/131.07; //131.07 is just 32768/250 to get us our 1deg/sec value
      GY = ((float)CGY-GYoff)/131.07;
      GZ = ((float)CGZ-GZoff)/131.07; 
    
     // sprintf(buf, "AX:%f, AY:%f, AZ:%f || GX:%f, GY:%f, GZ:%f,\t", AX,AY,AZ,GX,GY,GZ);
     //   sprintf(buf, "GX:%f, GY:%f, GZ:%f,\t\r\n", GX,GY,GZ);
    
     // SERIAL_UartPutString(buf);
    
    // CyDelay(25);
      //From here you will want to look at complimentary filters to combine the values
    //and to try and stop the drift which will inevitably happen on the gyroscope...
    
   //assign pointers to ref. address, then de-ref.
    ptrAX = &AX;
    ptrAY = &AY;
    ptrAZ = &AZ;
    ptrGX = &GX;
    ptrGY = &GY;
    ptrGZ = &GZ;
    
    ax = *ptrAX;
    ay = *ptrAY;
    az = *ptrAZ;
    gx = *ptrGX;
    gy = *ptrGY;
    gz = *ptrGZ;
    
    MadgwickAHRSupdateIMU(gx, gy, gz, ax, ay, az);
    
 //   yaw = atan2(2.0f * (q[1] * q[2] + q[0] * q[3]), q[0] * q[0] + q[1] * q[1] - q[2] * q[2] - q[3] * q[3]);
//    pitch = -asin(2.0f * (q[1] * q[3] - q[0] * q[2]));
 //   roll = atan2(2.0f * (q[0] * q[1] + q[2] * q[3]), q[0] * q[0] - q[1] * q[1] - q[2] * q[2] + q[3] * q[3]);
 //   pitch *= 180.0f / PI;
 //   yaw *= 180.0f / PI;
 //   roll *= 180.0f / PI;  
    
    yaw = atan2(2*q1*q2-2*q0*q3,2*q0*q0+2*q1*q1-1)*180/PI;
    pitch = -1*asin(2*q1*q3+2*q0*q2)*180/PI;
    roll = atan2(2*q2*q3-2*q0*q1,2*q0*q0+2*q3*q3-1)*180/PI;
        
   //  sprintf(buf,"gx:%f, gy:%f, gz:%f,\t\r\n", gx, gy, gz);
   //  SERIAL_UartPutString(buf);
     
  //  sprintf(buf,"qo:%f, g1:%f, q2:%f, q3:%f \t\r\n", q0, q1, q2, q3);
    
    sprintf(buf,"roll:%f, pitch:%f, yaw:%f,\t\r", roll, pitch, yaw);
    SERIAL_UartPutString(buf);
    CyDelay(25);
     
    }
    
    
}
