/*******************************************************************************
* File Name: main.c
*
* Version:   1.0
*
* Description:
*  This example shows how to use the DMA to transfer data from a RAM array to
*  the SPI TX buffer, and shows how to use the DMA to transfer data from the SPI
*  RX buffer to a RAM array.
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation. All rights reserved.
* This software is owned by Cypress Semiconductor Corporation and is protected
* by and subject to worldwide patent and copyright laws and treaties.
* Therefore, you may use this software only as provided in the license agreement
* accompanying the software package from which you obtained this software.
* CYPRESS AND ITS SUPPLIERS MAKE NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
* WITH REGARD TO THIS SOFTWARE, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT,
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*******************************************************************************/

#include <project.h>
#include <string.h>

//**** nrF24L0
#include <nrf24l01_regmap.h>
//**** nrF24L0

#define     DESCR0              0
#define     DESCR1              1
#define     TX_BUFFER_SIZE      1
#define     RX_BUFFER_SIZE      2
#define     SPI_2B_MASK         0x1F // 0001 1111

/* LED control defines. LED is active low. */
#define     LED_ON              0
#define     LED_OFF             1
/* Radio Enable Defines.  Radio is active high */
#define     NRF24_DISABLE              0
#define     NRF24_ENABLE             1

uint16 spi_rx_data=0;
uint32 spi_rx_data_size=0;
/* loop counter */
uint8 ccount = 0;
uint32 myint = 0xFAFA;
uint16 spi_w_comm = 0;
uint16 tx_send_success=0;

uint8 status_reg =0;


/*******************************************************************************
* Function Name: main
********************************************************************************
*
* Summary:
*  This function starts the SPI communication, configures and starts DMA
*  transfers. Once data exchange completes, checks the content of sent and
*  received data.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/


void writeNRF24_address_value(uint8 address, uint8 value){
    uint16 debug_write=0;
    //address should be set to 0x05 (or RF_CH)
    
    
    
    uint8 W_REGISTER = 0x20;
    
    uint16 writeAddressValue = (W_REGISTER | ( SPI_2B_MASK & address ));
            
        // W_REG = 0010 0000
        // address = 0000 0101
        // SPI_2B_MASK = 0001 1111
    
        // SPI_2B_MASK and address = 0000 0101
        // final = 0010 0101
    
            
          
    
    uint8 newRegisterSetting = value;
                        //0001 0010 or whatever value is
    debug_write = (writeAddressValue << 8 ) | newRegisterSetting;
    SPIM_SpiUartWriteTxData((writeAddressValue << 8 ) | newRegisterSetting);
                        // add the two together
                        // shift writeAddressValue left 8 
                        //0010 0101 0000 0000 or 0000 0000 0001 0010
                        //0011 0010 0001 0010 <---- write that to slave
    status_reg=SPIM_SpiUartReadRxData();
     SPIM_SpiUartClearTxBuffer();
    SPIM_SpiUartClearRxBuffer();
}
void writeNRF24_tx_byte(uint8 value){
    chip_en_Write(0x00);
      CyDelay(1);
    uint16 debug_write=0;
    uint16 tx_write=0;
   
    tx_write=WR_NAC_TX_PLOAD << 8 | value;

            
          
    
    
    SPIM_SpiUartWriteTxData(tx_write);
                        // add the two together
                        // shift writeAddressValue left 8 
                        //0010 0101 0000 0000 or 0000 0000 0001 0010
                        //0011 0010 0001 0010 <---- write that to slave
    SPIM_SpiUartClearRxBuffer();
    chip_en_Write(0x01);
    CyDelay(1);
    chip_en_Write(0x00);
   // tx_send_success=readNRF24_address_value(0x07) & 0x32;
    
}

uint16 readNRF24_address_value(uint8 address){
  
    uint16 spi_rx_data_local;
    uint8 value=0;
    
    SPIM_SpiUartClearRxBuffer();
    
     uint16 writeAddressValue = (RF24_R_REGISTER| ( SPI_2B_MASK & address ));
            

    
    SPIM_SpiUartWriteTxData(writeAddressValue << 8 | 0xFF);
                        // add the two together
                        // shift writeAddressValue left 8 
                        //0010 0101 0000 0000 or 0000 0000 0001 0010
                        //0011 0010 0001 0010 <---- write that to slave
    // SPIM_SpiUartWriteTxData(0xFF); // send any old byte to get the data out
    spi_rx_data_local=SPIM_SpiUartReadRxData();  //here's the data coming out
    return(spi_rx_data_local);
}
int main()
{
    /* take Chip Enable pin low on nRF */
    chip_en_Write(NRF24_DISABLE);
    /* Start SPI communication. */
    
     
    SPIM_Start();

 
    SPIM_SpiUartWriteTxData(0x00);
   
    spi_rx_data_size=SPIM_SpiUartGetRxBufferSize();
    
    
    spi_rx_data=SPIM_SpiUartReadRxData();  //here's the data coming out

    SPIM_SpiUartClearRxBuffer();
    
    writeNRF24_address_value(RF_CH, 0x60); //currently working!! (debug r0 register outputs correct binary val)
   // SPIM_SpiUartWriteTxData(0x05<<8);
 
    spi_rx_data=readNRF24_address_value(RF_CH);
    
writeNRF24_address_value(RF_SETUP, 0x32); // set data rate and power
    
    // writeNRF24_address_value(TX_ADDR, 0xE7); // set pipe
    writeNRF24_address_value(FIFO_STATUS, 0x08); //
    writeNRF24_address_value(RX_PW_P0,0x01); // 1 byte payload
    writeNRF24_address_value(FEATURE,0x01); // set TX NO ACK Feature
    writeNRF24_address_value(CONFIG,0x0A);
    chip_en_Write(NRF24_ENABLE);
    tx_send_success=readNRF24_address_value(0x07);
    while(1>0){
    writeNRF24_address_value(FLUSH_TX,0x00);
    writeNRF24_tx_byte(0x50); 
    tx_send_success=readNRF24_address_value(0x07);
    }
 
    
//    spi_rx_data_size=SPIM_SpiUartGetRxBufferSize();
//    
//    
//    spi_rx_data=SPIM_SpiUartReadRxData();
//    
//    
//    SPIM_SpiUartWriteTxData(0x0200); // write config register
//   
//    spi_rx_data_size=SPIM_SpiUartGetRxBufferSize();
//    
//    SPIM_SpiUartClearRxBuffer();
//    
//      spi_rx_data=SPIM_SpiUartReadRxData();
//    
//    SPIM_SpiUartWriteTxData(0x0000); // 
//   
//    spi_rx_data_size=SPIM_SpiUartGetRxBufferSize();
//    
//
//    spi_rx_data=SPIM_SpiUartReadRxData();
//    
//    SPIM_SpiUartWriteTxData(0xFF); // send any old byte to get the data out
//    spi_rx_data=SPIM_SpiUartReadRxData();  //here's the data coming out
//    
//    SPIM_SpiUartWriteTxData(0xFF); // send any old byte to get the data out
//    spi_rx_data=SPIM_SpiUartReadRxData();  //here's the data coming out
//    SPIM_SpiUartWriteTxData(0xFF); // send any old byte to get the data out
//    spi_rx_data=SPIM_SpiUartReadRxData();  //here's the data coming out
//    SPIM_SpiUartWriteTxData(0xFF); // send any old byte to get the data out
//    spi_rx_data=SPIM_SpiUartReadRxData();  //here's the data coming out
//    myint =  spi_rx_data_size &  (  spi_rx_data & RF24_REGISTER_MASK  );
    
    ;
    
    
}

/* [] END OF FILE */
