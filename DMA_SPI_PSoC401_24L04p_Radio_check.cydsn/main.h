/*******************************************************************************
* File Name: main.h
*
* Version: 1.0
*
* Description:
*  This file provides function prototypes, constants and macros for the
*  SCB SPI Master example project.
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation. All rights reserved.
* This software is owned by Cypress Semiconductor Corporation and is protected
* by and subject to worldwide patent and copyright laws and treaties.
* Therefore, you may use this software only as provided in the license agreement
* accompanying the software package from which you obtained this software.
* CYPRESS AND ITS SUPPLIERS MAKE NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
* WITH REGARD TO THIS SOFTWARE, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT,
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*******************************************************************************/

#if !defined(CY_MAIN_H)
#define CY_MAIN_H
#endif
#include <stdint.h>
#include <project.h>


/***************************************
*            Constants
****************************************/

/* Packet size */
#define PACKET_SIZE      (1u)

/* Byte position within the packet */
#define PACKET_SOP_POS  (0u)
#define PACKET_CMD_POS  (1u)

/* Command and status share the same offset */
#define PACKET_STS_POS  (PACKET_CMD_POS)
#define PACKET_EOP_POS  (2u)

/* Start and end of the packet markers */
#define PACKET_SOP      (0x01u)
#define PACKET_EOP      (0x17u)

/* Command execution status */
#define STS_CMD_DONE    (0x00u)
#define STS_CMD_FAIL    (0xFFu)


/* Delay between commands in milliseconds */
#define CMD_TO_CMD_DELAY  (500u)




/* [] END OF FILE */
