/*******************************************************************************
* File Name: main.c
*
* Version:   1.0
*
* Description:
*  This example shows how to use the DMA to transfer data from a RAM array to
*  the SPI TX buffer, and shows how to use the DMA to transfer data from the SPI
*  RX buffer to a RAM array.
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation. All rights reserved.
* This software is owned by Cypress Semiconductor Corporation and is protected
* by and subject to worldwide patent and copyright laws and treaties.
* Therefore, you may use this software only as provided in the license agreement
* accompanying the software package from which you obtained this software.
* CYPRESS AND ITS SUPPLIERS MAKE NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
* WITH REGARD TO THIS SOFTWARE, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT,
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*******************************************************************************/

#include <main.h>
#include <string.h>
#include <stdint.h>
//**** nrF24L01+
#include <config.h>
#include <nordic_common.h>
#include <hal_nrf.h>
#include <radio_init.h>
//**** nrF24L01+

#define     DESCR0              0
#define     DESCR1              1
#define     TX_BUFFER_SIZE      1
#define     RX_BUFFER_SIZE      2
#define     SPI_2B_MASK         0x1F // 0001 1111

/* LED control defines. LED is active low. */
#define     LED_ON              0
#define     LED_OFF             1
/* Radio Enable Defines.  Radio is active high */
#define     NRF24_DISABLE            0
#define     NRF24_ENABLE             1



uint16 spi_rx_data=0;
uint32 spi_rx_data_size=0;
/* loop counter */
uint8 ccount = 0;

/* debug */
uint32 myint = 0xFAFA;
uint16 spi_w_comm = 0;
uint16 tx_send_success=0;

uint8 status_reg =0;
uint8 reg_temp1 = 0x00;
uint8 reg_temp2 = 0x00;



/* SPIM functions */



/* This dummy buffer used by SPIM when it receives status packet.
* This dummy buffer used by SPIS when it receives command packet.
*/
const uint8 dummyBuffer[PACKET_SIZE] = {0xFFu};
<<<<<<< HEAD
const uint16 wrBuf[9u] = {0xA0,0x50,0x50,0x50,0x50,0x50,0x50,0x50,0x50};
=======



>>>>>>> c699cd22aa6bdce21a4e301af5cf02f5502c9579



/*******************************************************************************
* Function Name:writeNRF24_address_value
********************************************************************************/
void writeNRF24_address_value(uint8 address, uint8 value){
    // uint16 debug_write=0;
  
    uint8 W_REGISTER = 0x20;
    
    uint16 writeAddressValue = (W_REGISTER | ( SPI_2B_MASK & address ));
            
        // W_REG = 0010 0000
        // address = 0000 0101
        // SPI_2B_MASK = 0001 1111
    
        // SPI_2B_MASK and address = 0000 0101
        // final = 0010 0101
    
            
          
    
    uint8 newRegisterSetting = value;
                        //0001 0010 or whatever value is
    // debug_write = (writeAddressValue << 8 ) | newRegisterSetting;
    SPIM_SpiUartWriteTxData((writeAddressValue << 8 ) | newRegisterSetting);
                        // add the two together
                        // shift writeAddressValue left 8 
                        //0010 0101 0000 0000 or 0000 0000 0001 0010
                        //0011 0010 0001 0010 <---- write that to slave 
    while(PACKET_SIZE > SPIM_SpiUartGetRxBufferSize())
    {
    }
    status_reg=SPIM_SpiUartReadRxData();
    SPIM_SpiUartClearTxBuffer();
    SPIM_SpiUartClearRxBuffer();
}

/*******************************************************************************
* Function Name: writeNRF24_reset
********************************************************************************/
void writeNRF24_reset(){
    /* Make sure the NRF is in the same state as after power up
            to avoid problems resulting from left over configuration
            from other programs.
    */
    writeNRF24_address_value(CONFIG,0x08);
    writeNRF24_address_value(EN_AA,0x3F);
    writeNRF24_address_value(EN_RXADDR,0x02);
    writeNRF24_address_value(SETUP_AW,0x03);
    writeNRF24_address_value(SETUP_RETR,0x03);
    writeNRF24_address_value(RF_CH,0x02);
    writeNRF24_address_value(RF_SETUP,0x06);
    /* RX_ADDR_P0 is a 5 byte register Should be at power up default of 0xE7E7E7E7E7
       RX_ADDR_P1 is a 5 byte register Should be at power up default of 0xC2C2C2C2C2
    */
    writeNRF24_address_value(RX_ADDR_P2,0xc3);
    writeNRF24_address_value(RX_ADDR_P3,0xc4);
    writeNRF24_address_value(RX_ADDR_P4,0xc5);
    writeNRF24_address_value(RX_ADDR_P5,0xc6);
    /* TX_ADDR is a 5 byte register Should be at power up default of 0xE7E7E7E7E7
    */
    writeNRF24_address_value(RX_PW_P0,0x00);
    writeNRF24_address_value(RX_PW_P1,0x00);
    writeNRF24_address_value(RX_PW_P2,0x00);
    writeNRF24_address_value(RX_PW_P3,0x00);
    writeNRF24_address_value(RX_PW_P4,0x00);
    writeNRF24_address_value(RX_PW_P5,0x00);
    writeNRF24_address_value(DYNPD,0x00);
    writeNRF24_address_value(FEATURE,0x00);
    
}
/*******************************************************************************
* Function Name: writeNRF24_reset
********************************************************************************/
void writeNRF24_reset(void){
    /* Make sure the NRF is in the same state as after power up
            to avoid problems resulting from left over configuration
            from other programs.
    */
    writeNRF24_address_value(CONFIG,0x08);
    writeNRF24_address_value(EN_AA,0x3F);
    writeNRF24_address_value(EN_RXADDR,0x02);
    writeNRF24_address_value(SETUP_AW,0x03);
    writeNRF24_address_value(SETUP_RETR,0x03);
    writeNRF24_address_value(RF_CH,0x02);
    writeNRF24_address_value(RF_SETUP,0x06);
    /* RX_ADDR_P0 is a 5 byte register Should be at power up default of 0xE7E7E7E7E7
       RX_ADDR_P1 is a 5 byte register Should be at power up default of 0xC2C2C2C2C2
    */
    writeNRF24_address_value(RX_ADDR_P2,0xc3);
    writeNRF24_address_value(RX_ADDR_P3,0xc4);
    writeNRF24_address_value(RX_ADDR_P4,0xc5);
    writeNRF24_address_value(RX_ADDR_P5,0xc6);
    /* TX_ADDR is a 5 byte register Should be at power up default of 0xE7E7E7E7E7
    */
    writeNRF24_address_value(RX_PW_P0,0x00);
    writeNRF24_address_value(RX_PW_P1,0x00);
    writeNRF24_address_value(RX_PW_P2,0x00);
    writeNRF24_address_value(RX_PW_P3,0x00);
    writeNRF24_address_value(RX_PW_P4,0x00);
    writeNRF24_address_value(RX_PW_P5,0x00);
    writeNRF24_address_value(DYNPD,0x00);
    writeNRF24_address_value(FEATURE,0x00);
    
}



/*******************************************************************************
* Function Name: writeNRF24_tx_byte
********************************************************************************/
void writeNRF24_tx_byte(uint8 value){
    /* send TX Byte value over non-acknowledged transmit path */
    chip_en_Write(0x00);
    // uint16 debug_write=0;
    uint16 tx_write=0;
   
    tx_write=WR_NAC_TX_PLOAD << 8 | value;
    
    SPIM_SpiUartWriteTxData(tx_write);
                        // add the two together
                        // shift writeAddressValue left 8 
                        //0010 0101 0000 0000 or 0000 0000 0001 0010
                        //0011 0010 0001 0010 <---- write that to slave
     while(PACKET_SIZE > SPIM_SpiUartGetRxBufferSize())
    {
    }
    SPIM_SpiUartClearRxBuffer();
    chip_en_Write(0x01);
    CyDelay(1);
    chip_en_Write(0x00);
   
    
}


/*******************************************************************************
* Function Name: readNRF24_address_value
********************************************************************************/
uint16 readNRF24_address_value(uint8 address){
  
    uint16 spi_rx_data_local;
    // uint8 value=0;
    
    SPIM_SpiUartClearRxBuffer();
    
     uint16 writeAddressValue = (RF24_R_REGISTER| ( SPI_2B_MASK & address ));
            

    
    SPIM_SpiUartWriteTxData(writeAddressValue << 8 | 0xFF);
                        // shift writeAddressValue left 8 
                        // or the two together:
                        //0011 0010 0001 0010 <---- write that to slave
                        //0010 0101 0000 0000 or 0000 0000 0001 0010
    
    while(PACKET_SIZE > SPIM_SpiUartGetRxBufferSize())  
    {
        // Needed after every write to know write has finished
    }
    spi_rx_data_local=SPIM_SpiUartReadRxData();  //read value
    return(spi_rx_data_local);
}

int main()
{
    /* take Chip Enable pin low on nRF */
    chip_en_Write(NRF24_DISABLE);
    /* Start SPI communication. */
    
   
    /* Get SPI going */
    SPIM_Start();
    
    CyGlobalIntEnable;
 
    SPIM_SpiUartWriteTxData(0x00);
    spi_rx_data_size=SPIM_SpiUartGetRxBufferSize();
<<<<<<< HEAD
    
    
    spi_rx_data=SPIM_SpiUartReadRxData();  

=======
    spi_rx_data=SPIM_SpiUartReadRxData();  //here's the data coming out
>>>>>>> c699cd22aa6bdce21a4e301af5cf02f5502c9579
    SPIM_SpiUartClearRxBuffer();
    
    writeNRF24_reset();
    
<<<<<<< HEAD
    writeNRF24_address_value(RF_CH, 0x60); 
 
    spi_rx_data=readNRF24_address_value(RF_CH);
    
    writeNRF24_address_value(RF_SETUP, 0x26);   // set data rate to 250 Kbps and power to max
    spi_rx_data=readNRF24_address_value(RF_SETUP);
    writeNRF24_address_value(SETUP_RETR,0xFF); // wait 4000us & re-transmit count to 15 times
    writeNRF24_address_value(EN_AA,0x00);      // Disable enhanced shockburst
    // writeNRF24_address_value(TX_ADDR, 0xE7);   // set pipe
    writeNRF24_address_value(FIFO_STATUS, 0x08); //
    writeNRF24_address_value(RX_PW_P0,0x08); // 8 byte payload
    spi_rx_data=readNRF24_address_value(RX_PW_P0);
    writeNRF24_address_value(RX_PW_P1,0x08); // 8 byte payload
    spi_rx_data=readNRF24_address_value(RX_PW_P1);
    writeNRF24_address_value(FEATURE,0x00); // set TX NO ACK Feature
    writeNRF24_address_value(CONFIG,0x0E);
    
    /* start listening */
    reg_temp1=readNRF24_address_value(CONFIG);
    reg_temp2=reg_temp1 | PWR_UP | PRIM_RX ;
    writeNRF24_address_value(CONFIG,reg_temp2);
    
    reg_temp1=readNRF24_address_value(STATUS);
    reg_temp2=reg_temp1 | RX_DR | MAX_RT | TX_DS;
    writeNRF24_address_value(STATUS,reg_temp2);
    
    CyDelay(1);
    reg_temp1=readNRF24_address_value(CONFIG);
    reg_temp2=reg_temp1 &  ~PRIM_RX ;
    writeNRF24_address_value(CONFIG,reg_temp2);
    
=======
    writeNRF24_address_value(RF_SETUP,0x06); /* power max, 1 Mbps */
    spi_rx_data=readNRF24_address_value(RF_CH);   /* debug */
    
    writeNRF24_address_value(CONFIG,0x0C); /* enable 2 byte CRC power down */
    
    writeNRF24_address_value(RF_SETUP, 0x26); /* set data rate to 250kbps and power to max */
    
    spi_rx_data=readNRF24_address_value(SETUP_AW);
    
    writeNRF24_address_value(TX_ADDR, 0xE7); // set pipe
    writeNRF24_address_value(FIFO_STATUS, 0x08); //
    writeNRF24_address_value(RX_PW_P0,0x01); // 1 byte payload
    writeNRF24_address_value(FEATURE,0x01); // set TX NO ACK Feature
    writeNRF24_address_value(CONFIG,0x02);
    chip_en_Write(NRF24_ENABLE);
    tx_send_success=readNRF24_address_value(0x07);
>>>>>>> c699cd22aa6bdce21a4e301af5cf02f5502c9579
    while(1>0){
    SPIM_SpiUartPutArray(wrBuf,9u);  // load tx fifo
    chip_en_Write(NRF24_ENABLE);
    CyDelay(1);
    chip_en_Write(NRF24_DISABLE);
    tx_send_success=readNRF24_address_value(STATUS);
    writeNRF24_address_value(FLUSH_TX,0x00);
    }
 
<<<<<<< HEAD
    
//    spi_rx_data_size=SPIM_SpiUartGetRxBufferSize();
//    
//    
//    spi_rx_data=SPIM_SpiUartReadRxData();
//    
//    
//    SPIM_SpiUartWriteTxData(0x0200); // write config register
//  
//    spi_rx_data_size=SPIM_SpiUartGetRxBufferSize();
//    
//    SPIM_SpiUartClearRxBuffer();
//    
//      spi_rx_data=SPIM_SpiUartReadRxData();
//    
//    SPIM_SpiUartWriteTxData(0x0000); // 
//   
//    spi_rx_data_size=SPIM_SpiUartGetRxBufferSize();
//    
//
//    spi_rx_data=SPIM_SpiUartReadRxData();
//    
//    SPIM_SpiUartWriteTxData(0xFF); // send any old byte to get the data out
//    spi_rx_data=SPIM_SpiUartReadRxData();  //here's the data coming out
//    
//    SPIM_SpiUartWriteTxData(0xFF); // send any old byte to get the data out
//    spi_rx_data=SPIM_SpiUartReadRxData();  //here's the data coming out
//    SPIM_SpiUartWriteTxData(0xFF); // send any old byte to get the data out
//    spi_rx_data=SPIM_SpiUartReadRxData();  //here's the data coming out
//    SPIM_SpiUartWriteTxData(0xFF); // send any old byte to get the data out
//    spi_rx_data=SPIM_SpiUartReadRxData();  //here's the data coming out
//    myint =  spi_rx_data_size &  (  spi_rx_data & RF24_REGISTER_MASK  );
    
    ;
    
    
=======

>>>>>>> c699cd22aa6bdce21a4e301af5cf02f5502c9579
}

/* [] END OF FILE */
